package cs224n.wordaligner;  

import cs224n.util.*;
import java.util.List;

import java.util.*;

/**
 * WordAligner implementation based on IBM 2 Model
 */

public class IBM2WordAligner implements WordAligner {
  
  private CounterMap<String,String> sourceTargetProbabilities;
  private CounterMap<Integer, Triplet<Integer, Integer, Integer>> positionLengthProbabilities;
  
  private Random rgen;
  
  public IBM2WordAligner() {
    rgen = new Random();
    positionLengthProbabilities = new CounterMap<Integer, Triplet<Integer, Integer, Integer>>();
  }

  public Alignment align(SentencePair sentencePair) {
    Alignment alignment = new Alignment();
    List<String> sourceWords = sentencePair.getSourceWords();
    List<String> targetWords = sentencePair.getTargetWords();
    int numSourceWords = sentencePair.getSourceWords().size();
    int numTargetWords = sentencePair.getTargetWords().size();
    
    for(int i = 0; i < numTargetWords; i++) {
      double bestProbSoFar = 0;
      int bestIndexSoFar = -1;
      
      Triplet<Integer, Integer, Integer> lengthTriplet = new Triplet(i, numTargetWords, numSourceWords);
      for(int j = 0; j < numSourceWords; j++) {
        double probability = sourceTargetProbabilities.getCount(sourceWords.get(j), targetWords.get(i));
        
        probability *= positionLengthProbabilities.getCount(j, lengthTriplet);
        
        if(probability >= bestProbSoFar) {
          bestProbSoFar = probability;
          bestIndexSoFar = j;
        }
      }
      
      alignment.addPredictedAlignment(i, bestIndexSoFar);
    }
    
    return alignment;
  }

public void train(List<SentencePair> trainingPairs) {
    IBM1WordAligner ibm1wordAligner = new IBM1WordAligner();
    ibm1wordAligner.train(trainingPairs);
    sourceTargetProbabilities = ibm1wordAligner.getSourceTargetProbabilities();

    CounterMap<String, String> sourceTargetCounts = new CounterMap<String, String>();
    CounterMap<Integer, Triplet<Integer, Integer, Integer>> positionLengthCounts = new CounterMap<Integer, Triplet<Integer, Integer, Integer>>();
    double initialProbability = .5;
    
    for(SentencePair pair : trainingPairs) {
      List<String> targetWords = pair.getTargetWords();
      List<String> sourceWords = pair.getSourceWords();
      
      for(int i = 0; i < targetWords.size(); i++) {
        for(int j = 0; j < sourceWords.size(); j++) {  
          double randomDouble = rgen.nextDouble();
          positionLengthCounts.setCount(j, new Triplet(i, targetWords.size(), sourceWords.size()), 0);
          positionLengthProbabilities.setCount(j, new Triplet(i, targetWords.size(), sourceWords.size()), randomDouble);
        }
      }
    }
    
    for(int numIterations = 0; numIterations < 15; numIterations++) {
        System.out.println("IBM 2 Iteration " + numIterations);
        sourceTargetCounts = new CounterMap<String, String>();
        positionLengthCounts = new CounterMap<Integer, Triplet<Integer, Integer, Integer>>();
        for(SentencePair pair : trainingPairs){
          List<String> targetWords = pair.getTargetWords();
          List<String> sourceWords = pair.getSourceWords();
          int numTargetWords = targetWords.size();
          int numSourceWords = sourceWords.size();
                 
            for(int i = 0; i < numTargetWords; i++) {
                String targetWord = targetWords.get(i);
                double stDenom = 0, plDenom = 0;
                Triplet<Integer, Integer, Integer> lengthTriplet = new Triplet<Integer, Integer, Integer>(i, numTargetWords, numSourceWords);
                for(int j = 0; j < numSourceWords; j++) {
                    stDenom += sourceTargetProbabilities.getCount(sourceWords.get(j), targetWord);
                    plDenom += positionLengthProbabilities.getCount(j, lengthTriplet) * sourceTargetProbabilities.getCount(sourceWords.get(j), targetWord);
                }
            
                for(int j = 0; j < numSourceWords; j++) {
                
                    String sourceWord = sourceWords.get(j);
                    double stNum = sourceTargetProbabilities.getCount(sourceWord, targetWord);     
                    double plNum = positionLengthProbabilities.getCount(j, lengthTriplet) * sourceTargetProbabilities.getCount(sourceWord, targetWord);    
                    sourceTargetCounts.incrementCount(sourceWord, targetWord, stNum / stDenom);
                    positionLengthCounts.incrementCount(j, lengthTriplet, plNum / plDenom);
                }     
            }
        }
        
        sourceTargetProbabilities = Counters.conditionalNormalize(sourceTargetCounts);
        positionLengthProbabilities = Counters.conditionalNormalize(positionLengthCounts);
    }
  }
}