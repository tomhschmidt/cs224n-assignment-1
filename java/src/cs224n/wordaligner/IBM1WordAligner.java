package cs224n.wordaligner;  

import cs224n.util.*;
import java.util.List;

import java.util.*;

/**
 * WordAligner implementation based on IBM 1 Model
*/
 
public class IBM1WordAligner implements WordAligner {
  
  private CounterMap<String, String> sourceTargetProbabilities;
  
  public IBM1WordAligner() {
  }

  public Alignment align(SentencePair sentencePair) {
    Alignment alignment = new Alignment();
    List<String> sourceWords = sentencePair.getSourceWords();
    List<String> targetWords = sentencePair.getTargetWords();
    int numSourceWords = sentencePair.getSourceWords().size();
    int numTargetWords = sentencePair.getTargetWords().size();
    
    for(int i = 0; i < numTargetWords; i++) {
        double bestProbSoFar = 0;
        int bestIndexSoFar = -1;
        for(int j = 0; j < numSourceWords; j++) {
            double probability = sourceTargetProbabilities.getCount(sourceWords.get(j), targetWords.get(i));
            if(probability >= bestProbSoFar) {
                bestProbSoFar = probability;
                bestIndexSoFar = j;
            }
        }
        
        alignment.addPredictedAlignment(i, bestIndexSoFar);
    }
    
    return alignment;
  }

  public void train(List<SentencePair> trainingPairs) {
    CounterMap<String, String> sourceTargetCounts = new CounterMap<String, String>();
    Counter<String> sourceCounts = new Counter<String>();
    sourceTargetProbabilities = new CounterMap<String, String>();
    double initialProbability = .5;
    
    for(SentencePair pair: trainingPairs) {
        List<String> targetWords = pair.getTargetWords();
        List<String> sourceWords = pair.getSourceWords();
        
        sourceWords.add(NULL_WORD);
        
        for(String sourceWord : sourceWords) {
            for(String targetWord : targetWords) {
                sourceTargetProbabilities.setCount(sourceWord, targetWord, 1);
            }  
        }
    }
    
    sourceTargetProbabilities = Counters.conditionalNormalize(sourceTargetProbabilities);
    
    for(int i = 0; i < 15; i++) {
        System.out.println("IBM 1 Iteration " + i);
        sourceTargetCounts = new CounterMap<String, String>();
        for(SentencePair pair : trainingPairs){
          List<String> targetWords = pair.getTargetWords();
          List<String> sourceWords = pair.getSourceWords();
                 
            for(String targetWord : targetWords) {
                
                double denominator = 0;
                for(String sourceWord : sourceWords) {
                    denominator += sourceTargetProbabilities.getCount(sourceWord, targetWord);
                }
                
                for(String sourceWord : sourceWords) {
                    double numerator = sourceTargetProbabilities.getCount(sourceWord, targetWord);         
                    sourceTargetCounts.incrementCount(sourceWord, targetWord, numerator / denominator);
                    sourceCounts.incrementCount(sourceWord, numerator / denominator);
                }     
            }
        }
        
        sourceTargetProbabilities = Counters.conditionalNormalize(sourceTargetCounts);
    }
  }
  
  public CounterMap<String, String> getSourceTargetProbabilities() {
    return sourceTargetProbabilities;
  }
}
