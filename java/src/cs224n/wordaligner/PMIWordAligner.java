package cs224n.wordaligner;  

import cs224n.util.*;
import java.util.List;

/**
 * WordAligner implementation based on PMI Model
 */
 
public class PMIWordAligner implements WordAligner {
  
  private CounterMap<String,String> sourceTargetCounts = new CounterMap<String, String>();
  private Counter<String> sourceWordCounts = new Counter<String>();
  private Counter<String> targetWordCounts = new Counter<String>();
  private int numAligned = 0;

  public Alignment align(SentencePair sentencePair) {
    Alignment alignment = new Alignment();
    System.out.println(numAligned++);
    List<String> sourceWords = sentencePair.getSourceWords();
    List<String> targetWords = sentencePair.getTargetWords();
    int numSourceWords = sentencePair.getSourceWords().size();
    int numTargetWords = sentencePair.getTargetWords().size();
    
    double totalSourceWordsCount = sourceWordCounts.totalCount();
    double totalTargetWordsCount = targetWordCounts.totalCount();

    for(int i = 0; i < numTargetWords; i++) {
        double bestProbSoFar = 0;
        int bestIndexSoFar = -1;
        
        
        double targetWordProb = targetWordCounts.getCount(targetWords.get(i)) / totalTargetWordsCount;
        for(int j = 0; j < numSourceWords; j++) {
            double numerator = sourceTargetCounts.getCount(sourceWords.get(j), targetWords.get(i)) / totalSourceWordsCount;
            double denominator = sourceWordCounts.getCount(sourceWords.get(j)) / (totalSourceWordsCount * targetWordProb);
            double probability =  numerator / denominator;
            
            if(probability > bestProbSoFar) {
                bestProbSoFar = probability;
                bestIndexSoFar = j;
            }
        }
        
        alignment.addPredictedAlignment(i, bestIndexSoFar);
        
    }
    
    return alignment;
  }

  public void train(List<SentencePair> trainingPairs) {
    int numAlignedSentences = trainingPairs.size();
    sourceTargetCounts = new CounterMap<String,String>();
    
    for(SentencePair pair : trainingPairs){
      List<String> targetWords = pair.getTargetWords();
      List<String> sourceWords = pair.getSourceWords();
      sourceWords.add(NULL_WORD);
      
      for(String target : targetWords) {
        targetWordCounts.incrementCount(target, 1);
      }
      
      for(String source : sourceWords) {
        sourceWordCounts.incrementCount(source, 1);
        for(String target : targetWords) {
          sourceTargetCounts.incrementCount(source, target, 1.0);
        }
      }
      
    }
  }
}
