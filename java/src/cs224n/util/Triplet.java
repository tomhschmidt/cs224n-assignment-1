package cs224n.util;

/**
 * A generic-typed pair of objects.
 * @author Dan Klein
 */

public class Triplet<T, U, V>
{
   T a;
   U b;
   V c;

   public Triplet(T a, U b, V c)
   {
    this.a = a;
    this.b = b;
    this.c = c;
   }
   
    public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Triplet)) return false;

    final Triplet triplet = (Triplet) o;

    if (a != null ? !a.equals(triplet.a) : triplet.a != null) return false;
    if (b != null ? !b.equals(triplet.b) : triplet.b != null) return false;
    if (c != null ? !c.equals(triplet.c) : triplet.c != null) return false;

    return true;
  }
   
   public int hashCode() {
    int result;
    result = (a != null ? a.hashCode() : 0);
    result = 29 * result + (b != null ? b.hashCode() : 0);
    result = 31 * result + (c != null ? c. hashCode() : 0);
    return result;
  }

   T getA(){ return a;}
   U getB(){ return b;}
   V getC(){ return c;}
}